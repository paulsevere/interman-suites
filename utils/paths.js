const path = require('path');

const dataPath = path.resolve(__dirname, '../_data');

const stubs = path.resolve(dataPath, 'stubs.json');
const branch = path.resolve(dataPath, 'branch.json');

module.exports = { stubs, branch };
