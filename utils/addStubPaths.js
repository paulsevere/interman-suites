const execSync = require('child_process').execSync;
const paths = require('./paths');
const fs = require('fs-extra');

const isStubFile = file => /^stubs\//.test(file);

const getFiles = () => {
  const files = execSync('git ls-files', { encoding: 'utf-8' }).split('\n');

  return files
    .filter(isStubFile)
    .map(e => e.replace('stubs/', ''))
    .sort();
};

const updateStubsFile = files => fs.writeJSONSync(paths.stubs, files);

const exec = () => updateStubsFile(getFiles());

module.exports = exec;
