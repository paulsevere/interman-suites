#!/usr/bin/env node

const updateBranch = require('./updateBranch');
const addStubPaths = require('./addStubPaths');
const execSync = require('child_process').execSync;

const exec = () => {
  try {
    updateBranch();
    addStubPaths();
    execSync('git add -A');
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

exec();
