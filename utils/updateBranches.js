const fs = require('fs-extra');
const git = require('simple-git/promise')();

const paths = require('./paths');

const filterOriginRemotes = branches => branches.filter(branch => /^remotes\/origin/.test(branch)).map(branch => branch.replace('remotes/origin/', ''));

const getRemoteBranches = async () => {
  const { all } = await git.branch();
  return filterOriginRemotes(all);
};

const updateBranchesFile = branches => fs.writeJSON(paths.branches, branches);

const exec = async () => {
  return updateBranchesFile(await getRemoteBranches());
};

module.exports = exec;
