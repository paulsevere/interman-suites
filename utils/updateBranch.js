const fs = require('fs-extra');
const path = require('path');
const paths = require('./paths');

const gitDir = path.resolve(__dirname, '../.git/HEAD');

const getRefText = () => fs.readFileSync(gitDir, 'utf-8');

const getBranch = () =>
  getRefText()
    .replace('ref: refs/heads/', '')
    .trim('\n');

const writeBranch = () => fs.writeJSONSync(paths.branch, { branch: getBranch() }, 'utf-8');

module.exports = writeBranch;
